const express = require("express");
const app = express();

app.use(express.json());

const router = require("./routes/router")
app.use("/",router)

app.use((req,res,next) => {
    res.sendStatus(404);
  });
module.exports = app
