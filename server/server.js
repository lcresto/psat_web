
const app = require("./app")
const http = require("http")
const https = require("https")
const fs = require("fs")
const dotenv = require('dotenv')

dotenv.config();

const httpPort = process.env.HTTP_PORT
const httpsPort = process.env.HTTPS_PORT
const hostname = process.env.HOSTNAME
'use strict';

const { networkInterfaces } = require('os');

const nets = networkInterfaces();
const results = Object.create(null); // Or just '{}', an empty object
let addresses = ''

const privateKey = fs.readFileSync(process.env.PRIVKEY_PATH, 'utf-8');
const certificate = fs.readFileSync(process.env.CERT_PATH, 'utf-8');
const ca = fs.readFileSync(process.env.CA_PATH, 'utf-8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
}
// console.log(Object.keys(nets))
/*
let ipv4Found = false
if(Object.keys(nets).includes("Wi-Fi")){
    addresses = nets["Wi-Fi"]
    for(add of addresses){
        if(add.family == "IPv4"){
            hostname = add.address
            ipv4Found = true
        } 

    }
    if(!ipv4Found){
        throw("no ipv4 found")
    }
}
if(!ipv4Found){
    throw("no wifi found")
}*/
const httpServer = http.createServer(app).listen(httpPort, hostname, function(req, res){
    console.log("listening on http://" + hostname + ":" + httpPort)
    console.log("to go to the first website bright mode, go to : http://" + hostname + ":" + httpPort + "?website=1&theme=dark")
});
/*
const httpsServer = https.createServer(credentials, app).listen(httpsPort, hostname, function(req,res){
	console.log("listening on https://" + hostname + ":" + httpsPort)
}); */
/*
const server = http.createServer(app)
reload(app).then(function (res){
    server.listen(port, hostname, function(res, req){
        console.log("listening on " + hostname + ":" + port)
    });
}).catch(function (err){
    throw("reload doesn't work : "+err)
})
*/
