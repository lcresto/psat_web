const express = require("express");
const path = require("path");
const routes = express.Router();
const directory = path.join(__dirname, "../pages/");
const ressources_directory = path.join(__dirname, "../ressources");
const fs = require("fs");
let websiteJson = {}

const listOfWebsites =
{ 1 : {src: "https://www.lefigaro.fr/musique/en-australie-un-album-de-chant-d-oiseaux-en-voie-d-extinction-plane-en-tete-des-ventes-20211221",
      name: "lefigaro"},
  2 : {src: "https://www.allocine.fr/film/fichefilm_gen_cfilm=212358.html",
      name :"allocine"},
  3 : {src: "https://www.leparisien.fr/culture-loisirs/musique/la-chanteuse-angele-annonce-la-sortie-surprise-de-son-album-nonante-cinq-avec-une-semaine-davance-02-12-2021-34ESXU2Q25CRFM7SZC5LVWI2RM.php",
      name : "leparisien"},
  4 : {src: "https://www.20minutes.fr/planete/3152715-20211020-climat-rechauffement-menace-survie-millions-personnes-afrique-dit-onu",
      name: "20minutes"},
  5 : {src: "https://www.marmiton.org/recettes/recette_tarte-a-la-tomate-et-a-la-moutarde_20978.aspx",
      name: "marmiton"}}


async function writeInFile(data){
  fs.appendFile(
    "log_iframe.txt",
    JSON.stringify(data) + "\n",
    function (err) {
      if (err) {
        console.error(err);
      }
    }
  );
}
routes.get("/", (req,res) => {
  res.setHeader("Access-Control-Allow-Origin","*")
  websiteJson = {src: "", name:"", ads:""}
  // checks which website is chosen
  if(typeof(req.query.website) != "undefined" ){
    // create the header to put in the log file
    if(typeof(req.query.theme) != "undefined"){
      websiteJson.src = "created_page" //src is useless because we created the page 
      websiteJson.name = req.query.theme

    } else {
      websiteJson.src = listOfWebsites[req.query.website].src
      websiteJson.name = listOfWebsites[req.query.website].name

    }
  }  else {
      websiteJson.src = listOfWebsites[1].src
  }

  if(req.query.ads == "no") {
    websiteJson.ads = "no_ads"} 
  else {
    websiteJson.ads = "with_ads"
  }

  header = websiteJson.name + '__' + websiteJson.ads
  writeInFile(header)
 
  if(req.query.theme == "bright"){
    res.sendFile("20minutes_bright.html", {root: directory })
  } else{
    if(req.query.theme == "dark"){
      res.sendFile("20minutes_dark.html", {root: directory })

    } else {
      res.sendFile("different_pages.html", {root: directory })
    }
  }
})

//routes.get("/.well-known/acme-challenge/QnEG4PaHoD64LAAceHtg5u6CEe7UjyzyOzHKA92ze7I", (req,res) => {
//	res.sendFile("QnEG4PaHoD64LAAceHtg5u6CEe7UjyzyOzHKA92ze7I", {root: directory })
//})

routes.post("/data", (req, res) => {
  console.log(req.body);
  if(process.argv.length > 2){
    req.body.id = process.argv[2]
  } else {
    console.log("no id specified")
  }
  writeInFile(req.body)
  

  res.status(200).json({ data: "ok" });
});

routes.get("/src", (req,res) => {
  res.send(websiteJson.src)
})


module.exports = routes;
