

navigator.getBattery().then((res) => {
    let logJson = {
      id: id,
      ts: Math.floor(Date.now() / 1000),
      level: res.level,
      charging: res.charging,
      chargingTime: res.chargingTime,
      dischargingTime: res.dischargingTime,
    };

    log.textContent += JSON.stringify(logJson);

    fetch("/test_battery_iframe/data", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(logJson),
    });
  });